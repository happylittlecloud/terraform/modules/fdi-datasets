# Take
#
#   {
#     "cropimaging": {
#       "readers": ["blair", "jim"]
#       "writers": ["blair"]
#     },
#     "frontiercounts": {
#       "readers": ["jim"]
#     }
#   }
#
# and turn it into
#
#   {
#     "blair": merge(cropimaging.reader, cropimaging.writer),
#     "jim": merge(cropimaging.reader, frontiercounts.reader)
#   }
#
# I.e. "transpose" the datasets with the users and merge
# all the users permissions into a single policy
#


# Reader Policy
data "minio_iam_policy_document" "reader_policy_doc" {

  for_each = var.datasets

  statement {
    actions = [
      "s3:ListBucket",
      "s3:GetObject",
      "s3:ListAllMyBuckets",
    ]

    resources = [
      "arn:aws:s3:::${each.key}",
      "arn:aws:s3:::${each.key}/*",
    ]
  }
}


# Writer Policy
data "minio_iam_policy_document" "writer_policy_doc" {

  for_each = var.datasets

  statement {
    actions = [
      "s3:DeleteObject",
      "s3:GetBucketObjectLockConfiguration",
      "s3:PutObject",
    ]

    resources = [
      "arn:aws:s3:::${each.key}",
      "arn:aws:s3:::${each.key}/*",
    ]
  }
}


locals {
  reader_policies = transpose({
    for k, v in var.datasets : k => v.kubeflow_readers
  })

  writer_policies = transpose({
    for k, v in var.datasets : k => v.kubeflow_writers
  })

  all_keys = setunion(keys(local.writer_policies), keys(local.reader_policies))
}

data "aws_iam_policy_document" "combined" {
  for_each = local.all_keys
  source_policy_documents = setunion(
    try([for k, v in local.reader_policies[each.key]: data.minio_iam_policy_document.reader_policy_doc[v].json], []),
    try([for k, v in local.writer_policies[each.key]: data.minio_iam_policy_document.writer_policy_doc[v].json], [])
  )
}

# This is the end result we want!
resource "minio_iam_policy" "kubeflow_profile" {
  for_each = data.aws_iam_policy_document.combined
  name = "profile-${each.key}"
  policy = each.value.json
}

output "beelp" {
  value = data.minio_iam_policy_document.writer_policy_doc
}
